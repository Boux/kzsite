kzsite.service 'KZApiTop', ['KZApi', (KZApi) ->
  KZApi.Top = KZApi.resource '/records/top', {}, {}
  KZApi.Top.search = (str, params) ->
    params = _.extend
      tickrate: 128
      stage: 0
      has_teleports: false
      limit: 1200
      modes_list_string: 'kz_timer'
    , params

    if str.startsWith "STEAM_"
      params.steam_id = str
    else
      params.player_name = str

    return KZApi.Top.index params

  return KZApi.Top
]
.run ['KZApiTop', ->]
