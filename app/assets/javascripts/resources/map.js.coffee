kzsite.service 'KZApiMap', ['KZApi', (KZApi) ->
  return KZApi.Map = KZApi.resource '/maps/:id', { id: '@id' }, {}
]
.run ['KZApiMap', ->]
