window.kzsite = angular.module('kzsite', [
  'ngRoute',
  'ngResource',
  'ui.router'
])
.config(['$locationProvider', '$httpProvider', ($locationProvider, $httpProvider) ->
  $locationProvider.html5Mode false
  $locationProvider.hashPrefix ''

  # $httpProvider.defaults.headers.common['X-CSRF-Token'] = document.querySelector("meta[name='csrf-token']").content
])
