kzsite.service 'KZApi', ['$resource', '$http', 'KZ', ($resource, $http, KZ) ->
  BASE_URL = 'https://kztimerglobal.com/api/v1.0'
  DEFAULT_PARAMS = {}
  DEFAULT_ACTIONS =
    index:
      method: 'GET'
      isArray: true
    get:
      method: 'GET'
      isArray: false
    create:
      method: 'POST'
    update:
      method: 'PUT'
    destroy:
      method: 'DELETE'

  @resource = (url, params, actions) =>
    url = "#{BASE_URL}#{url}"

    params = _.extend {}, DEFAULT_PARAMS, params
    actions = _.extend {}, DEFAULT_ACTIONS, actions

    $resource url, params, actions

  return KZ.Api = @
]
.run ['KZApi', ->]
