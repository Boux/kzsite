kzsite.factory 'KZPlayer', ['KZ', (KZ) ->
  KZ.Player = class KZPlayer
    constructor: (key) ->
      @pending = 0
      @setKey key

    setKey: (key) =>
      @player_key = key
      @storage_key = if key then "#{key}_search" else null
      @search = localStorage[@storage_key] if @storage_key

    setSearch: (search = @search) =>
      @search = search
      localStorage[@storage_key] = @search if @storage_key

    hasTeleports: (has_teleports) =>
      @has_teleports = has_teleports

    refresh: (search = @search, params) =>
      return if !search

      params = _.extend {},
        has_teleports: !!@has_teleports
      , params

      @pending++
      KZ.Api.Top.search(search, params).$promise.then (top) =>
        @top = _.chain(top).sortBy('time').uniq('map_id').value()
        @top_by_map = _.indexBy @top, 'map_id'

        first = @top[0]
        return if !first

        @name = first.player_name
        @steam_id = first.steam_id
        @steamid64 = "#{first.steamid64}"
        @setSearch @steam_id
      .then =>
        @refreshRank()
      .finally =>
        @pending--

    refreshRank: =>
      return if !@steamid64

      # KZ.Api.PlayerRank.index(steamid64s: @steamid64, limit: 1).$promise.then (result) => @rank = result[0]
]
.run ['KZPlayer', ->]
