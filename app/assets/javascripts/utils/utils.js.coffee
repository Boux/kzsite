kzsite.service 'KZUtils', ['KZ', (KZ) ->
  KZ.Utils = @

  @tiers =
    1: "Very Easy"
    2: "Easy"
    3: "Medium"
    4: "Hard"
    5: "Very Hard"
    6: "Extreme"
    7: "Death"

  @mapTime = (time) =>
    return time if !time

    # hours, minutes, seconds
    splits = [60 * 60, 60, 1]


    currentTime = time
    timeStrs = []
    times = _.each splits, (s, i) =>
      val = Math.floor(currentTime / s)
      currentTime %= s
      return if !val && !timeStrs.length

      timeStrs.push @digits(val, if !timeStrs.length then 1 else 2)

    timeStrs.push '0' if !timeStrs.length

    ms = '.' + _.last "#{time.toFixed(3)}".split('.')
    return timeStrs.join(':') + ms

  @mapTier = (tier) =>
    @tiers[tier]

  @digits = (num = 0, minlength = 2) =>
    str = num.toString()

    while str.length < minlength
      str = '0' + str

    return str

  return @
]
.run ['KZUtils', ->]
