window.StoredObject = class StoredObject
  constructor: (key, object = {}, storage = localStorage) ->
    # return the basic object itself if the browser doesn't support Proxy
    return object if !Proxy

    @key = key
    @storage = storage
    @setDefaults object

    # create proxy object on self, when properties are set, direct them to @data instead
    return new Proxy @,
      get: (target, key) =>
        return @data[key]
      set: (target, key, value) =>
        @data[key] = value
        return true

  setDefaults: (object) =>
    @defaults = object
    @setObject _.defaults {}, @read(), object

  setObject: (object) =>
    @data = if !Proxy
      object
    else
      new Proxy object, @getStorageHandler()

  reset: (object = @defaults) =>
    @setObject object
    @store()

  # proxy handler for serializing @data to storage everytime it changes
  getStorageHandler: =>
    get: (target, key) =>
      return if typeof target[key] == 'object' and target[key] != null
        new Proxy target[key], @getStorageHandler()
      else
        target[key]
    set: (target, key, value) =>
      target[key] = value
      @store()
      return true

  read: (val = @storage[@key]) =>
    return if _.isString(val) then JSON.parse(val) else val

  write: (object = @data) =>
    return JSON.stringify object

  store: =>
    @storage[@key] = @write()
