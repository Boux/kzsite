kzsite.filter 'mapTime', ['KZ',
  (KZ) -> KZ.Utils.mapTime
]
kzsite.filter 'mapTier', ['KZ',
  (KZ) -> KZ.Utils.mapTier
]

kzsite.filter 'abs', [
  -> Math.abs
]
