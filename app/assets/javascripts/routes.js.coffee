kzsite.config ['$stateProvider', '$urlRouterProvider', ($stateProvider, $urlRouterProvider) ->
  $urlRouterProvider.otherwise '/'

  $stateProvider.state 'kzsite',
    url: '/'
    template: '<app-main></app-main>'

]
