kzsite.component 'appMain',
  templateUrl: 'components/app_main/app_main.html'
  controller: 'AppMainController'
  controllerAs: 'vm'

kzsite.controller 'AppMainController',
['$scope', '$element', 'KZ',
($scope, $element, KZ) ->
  @player = new KZ.Player 'main'
  @compares = []
  @maps = KZ.Api.Map.index(limit: 9999)
  @sort = { map: 'name' }

  defaultParams =
    has_teleports: true
    modes_list_string: 'kz_timer'
  @params = new StoredObject 'compare_params', defaultParams

  $scope.$watchCollection 'vm.compares', => @allPlayers = @getAllPlayers()

  @$onInit = =>
    @refreshPlayer()
    @addCompare()
    @maps.$promise.then @filterChanged

  @getAllPlayers = => [@player].concat @compares

  @refreshPlayer = (player = @player) =>
    params = _.mapObject defaultParams, (val, key) => @params[key]
    player.refresh(null, params)

  @setSort = (type, item) =>
    reverse = @sort[type] == item && !@sort.reverse
    @sort = {"#{type}": item, reverse: reverse}

  @filterChanged = =>
    @maps.$promise.then =>
      @filteredMaps = _.filter @maps, (m) => !@filter || m.name.toLowerCase().indexOf(@filter.toLowerCase()) != -1

  @setParam = (key, val) =>
    @params[key] = val
    @refreshPlayer p for p in @getAllPlayers()


  @addCompare = =>
    player = new KZ.Player()
    @compares.push player

  @removeCompare = (player) =>
    index = @compares.indexOf(player)
    return if index == -1

    @compares.splice(index, 1)




  @timeDiff = (time1, time2) =>
    diff = time2 - time1
    prefix = if diff >= 0 then "+" else "-"
    return "#{prefix} #{KZ.Utils.mapTime(Math.abs(diff))}"

  @timeGap = (time1, time2) =>
    return (Math.max(time1, time2) / Math.min(time1, time2)) - 1

  @isFastest = (player, map) =>
    return false if !_.filter(@compares, 'top_by_map').length

    fastboi = _.min @allPlayers, (p) => p.top_by_map?[map]?.time || Infinity
    return player == fastboi && !!fastboi.top_by_map?[map]?.time

  @sortTable = (map) =>
    if @sort.player && @sort.player.top_by_map
      return @sort.player.top_by_map?[map.id]?.time || (if @sort.reverse then 0 else Infinity)
    else if @sort.score == "points"
      return @player.top_by_map?[map.id]?.points || (if @sort.reverse then 0 else Infinity)
    else if @sort.score == "tp"
      return @player.top_by_map?[map.id]?.teleports || (if @sort.reverse then 0 else Infinity)
    else if @sort.map
      return map[@sort.map]




  return
]
